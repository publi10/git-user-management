import lib.gitlab as my_gitlab
import os
import sys


url = os.environ["URL"]
access_token = os.environ["ACCESS_TOKEN"]
group_id = os.environ["GROUP_ID"]
print(os.path.dirname(os.path.abspath(__file__)))

if __name__ == '__main__':
    gl = my_gitlab.my_gitlab(
        gitlab_url=url, access_token=access_token, script_dir=os.path.dirname(os.path.abspath(__file__)), top_level_group=group_id
    )
   # Checks the differences of user.yaml and existing users in the groups.
    try:
        gl.pull_user_to_yaml()
    except Exception as e:
        print("Error pulling users to yaml.")
        sys.exit(1)

    # try:
    gl.explain_differences(gl.compare_users())
    # print(gl.compare_users())
    # except Exception as e:
    #    print("Couldn't compare the structures, make sure setup is correct. If you run this script for the first time, use the artifact of this job to populate user.yaml and pull_user.yaml")
    #    sys.exit(1)
    # print(gl.prepare_changes(gl.compare_users()))
    # gl.user_changes(gl.prepare_changes(gl.compare_users()))
