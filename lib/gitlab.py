import gitlab
import yaml
from deepdiff import extract
from deepdiff import DeepDiff
import asyncio
import sys


class my_gitlab(gitlab.Gitlab):

    def __init__(self, gitlab_url, access_token, script_dir, top_level_group):
        super().__init__(url=gitlab_url, private_token=access_token)
        self.dir = script_dir
        self.top_level_group = top_level_group
        self.users_in_file = self.read_user_from_yaml()  # Users in user.yaml
        # Users fetched from gitlab server
        self.current_users = self.parse_user_to_yaml()

    def parse_user_to_yaml(self):
        """Getting current users for groups and projects as objects"""
        print("Pulling current users:")
        group_users = self.get_group_users()
        project_users = self.get_project_users()
        users = group_users + project_users
        return users

    def get_project_users(self):
        """Async pull of users from projects"""
        groups = self.groups.get(self.top_level_group,
                                 all=True, include_subgroups=True)
        tasks = []
        loop = asyncio.get_event_loop()

        sorted_groups = sorted(groups.descendant_groups.list(all=True),
                               key=lambda group: group.full_path)
        # print(sorted_groups)
        for group in sorted_groups:

            this_group = self.groups.get(group.id)
            for project in this_group.projects.list(all=True):
                tasks.append(asyncio.ensure_future(
                    self.create_asnyc_user_object(project, "Project")))
        print("Pulling ", str(len(tasks)), " projects")
        user_yaml = loop.run_until_complete(asyncio.gather(*tasks))

        # loop.close()
        return user_yaml

    def get_group_users(self):
        """Async pull of users from groups"""
        # try:
        tlg = self.groups.get(
            self.top_level_group, all=True, include_subgroups=True)
        tasks = []
        loop = asyncio.get_event_loop()

        sorted_groups = sorted(tlg.descendant_groups.list(all=True),
                               key=lambda group: group.full_path)

        sorted_groups.insert(0, tlg)
        # print(sorted_groups)
        for group in sorted_groups:
            tasks.append(asyncio.ensure_future(
                self.create_asnyc_user_object(group, "Group")))
        print("Pulling ", str(len(tasks)), " groups")
        user_yaml = loop.run_until_complete(asyncio.gather(*tasks))
        #user_yaml = asyncio.gather(*tasks)
        # sorted_user_yaml = sorted(user_yaml, key=lambda k: k[0])
        return user_yaml

    async def create_asnyc_user_object(self, item, type):
        """Takes the users from projects/groups from the server and parses them into the right format for the yaml structure"""
        user_yaml = {}
        if type == "Group":
            user_yaml[item.full_path] = {
                "type": "Group",
                "id": item.id,
                "users": self.parse_group_members(await self.get_group_members(item.id))
            }
        else:

            user_yaml[item.path_with_namespace] = {
                "type": "Project",
                "id": item.id,
                "users": self.parse_group_members(self.get_project_members(item.id))
            }
        # print(user_yaml)
        return user_yaml

    async def get_group_members(self, group_id):
        try:
            return self.groups.get(group_id).members.list()
        except Exception as e:
            print("!! Exception: ", e,
                  " - Could not get the group members for group ", group_id)
            sys.exit(1)

    def parse_group_members(self, group_members):
        members = []
        for member in group_members:
            members.append(self.parse_group_member(member))
        return members

    def parse_group_member(self, group_member):
        # print(group_member)
        return group_member.username + ":" + self.access_level_to_role(group_member.access_level)

    def access_level_to_role(self, access_level):
        cases = {
            10: "Guest",
            20: "Reporter",
            30: "Developer",
            40: "Maintainer",
            50: "Owner"
        }
        return cases.get(access_level)

    def role_to_access_level(self, role):
        cases = {
            "Guest": 10,
            "Reporter": 20,
            "Developer": 30,
            "Maintainer": 40,
            "Owner": 50
        }
        return cases.get(role)

    def get_project_members(self, project_id):
        try:
            return self.projects.get(project_id).members.list()
        except Exception as e:
            print("!! Exception: ", e,
                  " - Could not get the project members for project ", project_id)
            sys.exit(1)

    def read_user_from_yaml(self):
        """ Reads the desired user state from the user.yaml """
        try:
            with open(self.dir + "/users.yaml", "r") as file:
                users = yaml.safe_load(file)
                return users
        except Exception as e:
            print("!! Exception: ", e,
                  " - Could not read users.yaml file ")
            sys.exit(1)

    def pull_user_to_yaml(self):
        """Writes the current user state to yaml for synchronization to be stored as artifact"""
        try:
            with open(self.dir + "/lib/user_pull.yaml", "w") as outfile:
                yaml.dump(self.current_users, outfile,
                          default_flow_style=False)
        except Exception as e:
            print("!! Exception: ", e,
                  " - Could not dump users to user_pull.yaml ")
            sys.exit(1)

    def compare_users(self):
        """ Compare the differences between current users and desired user state using deepdiff
        """
        diff = DeepDiff(t1=self.current_users, t2=self.users_in_file, view='tree',
                        ignore_order=True, verbose_level=2)
        return diff

    def list_diffs(self, diff):
        list = []
        for key in diff:
            list.append(diff[key])
        return list

    def explain_differences(self, diff):
        """Will create a overview of changes to be done but will not change anything on gitlab side"""

        print("Differences:")
        if 'iterable_item_removed' in diff:
            for items in diff['iterable_item_removed']:
                print("- Remove : ", items.t1, "from repo: ", items.path())
        if 'iterable_item_added' in diff:
            for items in diff['iterable_item_added']:
                print("+ Add : ", items.t2, "from repo: ", items.path())
        if 'values_changed' in diff:
            for items in diff['values_changed']:
                try:
                    print("~ Changing : ", items.t1, " -> ", items.t2,
                          " from repo: ", items.up.up.path())
                except:
                    print("! Invalid Change : ", items.t1, " -> ",
                          items.t2, " from repo: ", items.path())

    def prepare_changes(self, diff):
        print("Preparing changes... ")
        if 'iterable_item_removed' in diff:
            for items in diff['iterable_item_removed']:
                self.handle_remove(items)
        if 'iterable_item_added' in diff:
            for items in diff['iterable_item_added']:
                self.handle_add(items)
        if 'values_changed' in diff:
            for items in diff['values_changed']:
                self.handle_change(items)

    def handle_remove(self, item):
        try:
            # Get the item from deepdiff by path --> Will extract item from tree structure to dict.
            parent = extract(self.current_users, item.up.up.path())
            if "type" in parent:
                self.remove_user(item, parent)
                print("- Removing : ", item.t1, " from ", item.path())
        except:
            # If no "type" in object it propably is a whole group that is not aligable to be changed by the script, therefor skipped.
            print("! Skipping : ", item.t1, " -> ", item.t2)

    def remove_user(self, item, parent):
        user = self.parse_role(item.t1)
        user_id = self.get_user_id(user['user'])

        if parent['type'] == "Group":
            target = self.groups.get(parent['id'])
        else:
            target = self.projects.get(parent['id'])
        try:
            target.members.delete(user_id)
        except Exception as e:
            raise Exception("Failed removing user " +
                            user['user'], "with exception ", e)

    def handle_add(self, item):
        try:
            parent = extract(self.current_users, item.up.up.path())

            if "type" in parent:
                self.add_user(item, parent)
                print("+ Adding : ", item.t2, " from ", item.path())
        except Exception as e:
            # If no "type" in object it propably is a whole group that is not eligable to be changed by the script, therefor skipped.
            print("! Skipping : ", item.t1, " -> ", item.t2,
                  " on ", item.path(), ", because of ", e)

    def add_user(self, item, parent):
        user = self.parse_role(item.t2)
        user_id = self.get_user_id(user['user'])
        user_access_level = user['role']

        if parent['type'] == "Group":
            target = self.groups.get(parent['id'])
        else:
            target = self.projects.get(parent['id'])
        try:
            result = target.members.create({'user_id': user_id,
                                            'access_level': user_access_level})
        except Exception as e:
            raise Exception(("Failed creating user " +
                            user['user'], " with exception ", e))

    def handle_change(self, item):
        try:
            parent = extract(self.current_users, item.up.up.path())
            t1_user = self.parse_role(item.t1)['user']
            t2_user = self.parse_role(item.t2)['user']

            if "type" in parent:
                if t1_user == t2_user:
                    self.modify_user(item, parent)
                    print("~ Modifying : ", item.t1, " -> ",
                          item.t2, " from ", item.path())
                else:
                    self.remove_user(item, parent)
                    self.add_user(item, parent)
                    print("~ Replace : ", item.t1, " -> ",
                          item.t2, " from ", item.path())

        except Exception as e:
            print("! Skipping : ", item.t1, " -> ", item.t2,
                  " on ", item.path(), ", with exception ", e)

    def modify_user(self, item, parent):
        user = self.parse_role(item.t2)
        user_id = self.get_user_id(user['user'])
        user_access_level = user['role']

        if parent['type'] == "Group":
            target = self.groups.get(parent['id'])
        else:
            target = self.projects.get(parent['id'])

        member = target.members.get(user_id)
        member.access_level = user_access_level
        member.save()

    def get_user_id(self, username):
        # Need to use search to work with different user attributes (name, username, id)
        users = self.users.list(search=username)
        if len(users) != 1:
            raise Exception(
                ("No or multiple users found for username : " + username))
        else:
            return users[0].id

    def parse_role(self, yaml_user):
        user = yaml_user.split(":")
        return {
            "user": user[0],
            "role": self.role_to_access_level(user[1])
        }
