import lib.gitlab as my_gitlab
import os

url = os.environ["URL"]
access_token = os.environ["ACCESS_TOKEN"]
group_id = os.environ["GROUP_ID"]


if __name__ == '__main__':
    gl = my_gitlab.my_gitlab(
        gitlab_url=url, access_token=access_token, script_dir=os.path.dirname(os.path.abspath(__file__)), top_level_group=group_id
    )

    # Checks the differences of user.yaml and existing users in the groups.
    # gl.explain_differences(gl.compare_users())
    # print(gl.prepare_changes(gl.compare_users()))
    # gl.user_changes(gl.prepare_changes(gl.compare_users()))
    gl.prepare_changes(gl.compare_users())
