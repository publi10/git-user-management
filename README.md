# git-users

This package is making user management with several subgroups a bit less tedious and much easier. Especially if your organization uses Gitlab groups for OIDC of large, nested repositories. It avoids clicking through uncounted directories and adding users manually everywhere.

*** 

## Overview
The package works in Gitlab CI. In general, it compares the changes in your user file with the actual user status and then removes or adds the changed user entities.
There are two CI Jobs: 
1. Sync: This job pulls the current user status and saves the file as an artifact. It can be used for initialization or if manual changes were done in the meanwhile. It also shows where your current file and the actual user status differ.
2. Change: This job will change the groups to the status defined in your ```users.yaml``` file.

> This package works in a declarative way. As long as there are differences between the actual state and the desired state (```users.yaml```) it will try to reach the desired state.


## 4-Eyes-Principle
Using this, you can implement a review of every user change by an independent authority: 
1. Protect the main branch if not yet done: Nobody should be allowed to push.
2. Protect the variables so they will only run on protected branches
3. Create a merge-request rule in the repo so another maintainer has to approve changes before they can be merged.
4. (Optional) You can also use code-owners to ensure that one specific person checks the changes on the ```users.yaml``` file. More about [Code-Owners](https://docs.gitlab.com/ee/user/project/code_owners.html).

This way, you could assure that another team colleague must review 
# Getting started

To implement this project you can simply fork it to your organization's repo.

## Setup

### Environment variables

- ```URL```: Your Gitlab instances' Url, e.g. ```https://my-gitlab.com```
- ```ACCESS_TOKEN```: The access token you have created for access to the API
- ```GROUP_ID```: The id of your top-level-group

> Currently this package only handles the subgroup membership and their descendant groups. Changes in the top-level group are not implemented. It could affect the inheritance to the lower groups badly.

### Sync Job
First of all, you should run the synch job in your pipeline. It will produce a file named ```pull_users.yaml``` as an artifact and show a list of differences. You can copy this file and add it to your repo as ```users.yaml```. 

You can rerun the sync job afterward and there should be no differences anymore.


## Workflow

### Sync Job

Change the members in ```user.yaml``` according to your desired member structure, commit and run the sync-job.
 
The sync job will point out differences like this: 

```sh

Differences:
- Remove :  {'my-repo/subgroup': {'type': 'Group', 'id': 2109, 'users': ['my.user@user.de:Developer']}} from repo:  root[3]
+ Add :  another.developer@devs.com:Maintainer from repo:  root[23]['my-repo/project-1']['users'][0]
~ Changing :  developer@devs.com:Owner  ->  developer@devs.com:Maintaner  from repo:  root[4]['my-repo/group-3']
```

- Line 1: You removed a whole group from ```user.yaml``` -> Will be ignored in live-run
- Line 2: You added a new developer to ```my-repo/project-1```
- Line 3: You changed the access level for a user

### Live run

If the sync-job is pointing out the differences as you wish, you can run the change-job.
It will try to achieve the desired status and will print outputs like this: 

```sh
- Removing :  devs@devs.de:Maintainer  from  root[11]['my-repo/subgroup-1']['users'][1]
! Skipping :  {'my-repo/subgroup': {'type': 'Group', 'id': 2109, 'users': ['my.user@user.de:Developer']}}  ->  not present
! Skipping :  not present  ->  devs@devs.com:Maintainer  on  root[23]['my-repo/subgroup-5']['users'][0] , because of  ('Failed creating user admin.devs@devs.com', ' with exception ', GitlabCreateError({'access_level': ['should be greater than or equal to Owner inherited membership from group my-repo']}))
+ Adding :  devs@devs:Maintainer  from  root[38]['my-repo/subgroup-1/project-1']['users'][1]
! Skipping :  dev.developer@devs.com:Owner  ->  dev.developer@devs.com:Maintaner  on  root[4]['my-repo/subgroup-1']['users'][2] , with exception  400: {'access_level': ['is not included in the list']}
~ Modifying :  dev.developer@devs.com:Developer  ->  dev.developer@devs.com:Maintainer  from  root[38]['my-repo/subgroup-1']['users'][0]
```

- Line 1: Remove a certain user from the repo
- Line 2: The group has been removed from ```user.yaml```. The change is skipped. Please correct ```user.yaml```.
- Line 3: You added a user that has higher authorizations through inheritance already
- Line 4: Adding a new user to a project
- Line 5: Skipped because typo in ```Maintaner```
- Line 6: User is modified to new access level


There are countless combinations of entities to add, remove, or undefined changes and we emphasize that you should use the export to synchronize your user data instead of manually adding new groups or projects to ```user.yaml```


```yaml
Groups:
  tlg/test/subgroup-1:
    id: 49586
    type: Group
    users:
    - my.username:Maintainer
  tlg/test/subgroup-2:
    id: 49587
    type: Group
    users: []
Projects:
  tlg/test/subgroup-1/subgroup-1-repo-1:
    id: 78440
    type: Project
    users: 
    - developer.user:Developer
  tlg/test/subgroup-1/subgroup-1-repo-2:
    id: 78445
    type: Project
    users: []

```
## Sandbox
If you want you can test the script by using the ```sandbox``` branch of the repository. You could e.g. run the sync job and download the file. Copy-paste it to ```users.yaml``` and play around with it by changing whatever you want.

## Roadmap

## Contributing
Feel free to improve the package.

## License
GNU General Public License version 3

